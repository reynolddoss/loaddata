from flask import Flask , jsonify
from flask_mongoengine import MongoEngine 
from flask_mongoengine.wtf import model_form
from mongoengine import *
import pandas as pd 


app=Flask(__name__)

db = MongoEngine(app)

class sports(db.Document):
	#sno     = db.IntField()
	OPP1  	= db.StringField()
	OPP2  	= db.StringField()
	REACH 	= db.IntField()
	CHANNEL = db.StringField()
	DATE	= db.DateTimeField()
	DAY		= db.StringField()




@app.route("/loaddata")
def loaddata():
	
	Final    = pd.read_csv("Final.csv",header=0)
	
	t_OPP1	 = list(Final.Opp1)
	t_OPP2	 = list(Final.Opp2)
	t_Reach	 = list(Final.Reach)
	t_Date 	 = list(Final.Date)
	t_Channel= list(Final.Channel)
	t_Day	 = list(Final.Day)

	for i in range(len(Final)):
		row = sports(OPP1=t_OPP1[i])
		row.OPP2=(t_OPP2[i])
		row.REACH=(t_Reach[i])
		row.CHANNEL=(t_Channel[i])
		row.DATE=(t_Date[i])
		row.DAY=(t_Day[i])
		row.save()
	return jsonify(status=True)



if __name__ == '__main__':
	app.run(debug=True)